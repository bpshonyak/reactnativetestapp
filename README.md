## Test App To Repro The Following Issue:
 https://github.com/CocoaPods/CocoaPods/issues/8138

 ## Repro Steps:

 1. Make sure you have Cocoapods 1.6.0.beta.1 installed
 2. Pull down the code
 3. `cd ios && pod install`
